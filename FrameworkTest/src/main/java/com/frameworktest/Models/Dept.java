package com.frameworktest.Models;

import com.frameworktest.DAO.DeptDAO;
import com.framework.Servlet.ServletMap;
import com.framework.Servlet.ViewModel;

import java.util.HashMap;

public class Dept {
    private final DeptDAO dao = new DeptDAO();
    private int id;
    private String nomDept;

    public Dept() {}

    public Dept(String nomDept) {
        setNomDept(nomDept);
    }

    public Dept(int id, String nomDept) {
        setId(id);
        setNomDept(nomDept);
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }

    public int getId() {
        return id;
    }

    public String getNomDept() {
        return nomDept;
    }

    @ServletMap(url = "show-dept")
    public ViewModel list() throws Exception {
        HashMap<String, Object> result = new HashMap<>();
        result.put("data", dao.list());
        return new ViewModel("List.jsp", result);
    }

    @ServletMap(url = "add-dept")
    public ViewModel insert(Dept d) throws Exception {
        dao.insert(d.getNomDept());
        return new ViewModel("index.jsp", null);
    }

    @ServletMap(url = "put-dept")
    public ViewModel update(Dept d, String nomDept) throws Exception {
        dao.update(d.getId(), nomDept);
        return new ViewModel("index.jsp", null);
    }

    @ServletMap(url = "supp-dept")
    public ViewModel delete(Dept d) throws Exception {
        dao.delete(d.getId());
        return new ViewModel("index.jsp", null);
    }
}