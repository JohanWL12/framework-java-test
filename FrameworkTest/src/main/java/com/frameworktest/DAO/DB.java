package com.frameworktest.DAO;


import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;

public class DB {
    private final Connection connect;
    private final Statement state;

    public DB() throws Exception {
        this.connect = DriverManager.getConnection("jdbc:postgresql://localhost:5432/dept", "postgres", "");
        this.state = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        this.connect.setAutoCommit(false);
    }

    public Connection getConnect() {
        return connect;
    }

    public Statement getStatement() {
        return this.state;
    }

    public void closeConnection() throws Exception {
        this.connect.close();
    }

    public void closeStatement() throws Exception {
        this.state.close();
    }
}
