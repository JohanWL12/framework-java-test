package com.frameworktest.DAO;

import com.frameworktest.Models.Dept;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;

public class DeptDAO {
    public List<Dept> list() throws Exception {
        DB db = null;
        Statement state = null;
        ResultSet result = null;
        List<Dept> list = new Vector<>();
        try {
            db = new DB();
            state = db.getStatement();
            result = state.executeQuery("SELECT * FROM dept");
            int i = 0;
            int column = result.getMetaData().getColumnCount();
            while (result.next()){
                for (int j = 0; j < column; j+=7) {
                    Dept d = new Dept(result.getInt(j+1), result.getString(j+2));
                    list.add(d);
                }
                i++;
            }
            return list;
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (result != null) {
                assert false;
                result.close();
            }
            if (state != null) {
                db.closeStatement();
            }
            if (db != null) {
                assert false;
                db.closeConnection();
            }
        }
    }

    public void insert(String nom) throws Exception {
        DB db = null;
        PreparedStatement state = null;
        String sql = "INSERT INTO dept(nomDept) VALUES (?)";
        try {
            db = new DB();
            state = db.getConnect().prepareStatement(sql);
            state.setString(1, nom);
            state.execute();
            db.getConnect().commit();
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (state != null) {
                state.close();
            }
            if (db != null) {
                assert false;
                db.closeConnection();
            }
        }
    }

    public void update(int id, String nom) throws Exception {
        DB db = null;
        PreparedStatement state = null;
        String sql = "UPDATE dept SET nomDept = ? WHERE iddept = ?";
        try {
            db = new DB();
            state = db.getConnect().prepareStatement(sql);
            state.setString(1, nom);
            state.setInt(2, id);
            state.execute();
            db.getConnect().commit();
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (state != null) {
                state.close();
            }
            if (db != null) {
                assert false;
                db.closeConnection();
            }
        }
    }

    public void delete(int id) throws Exception {
        DB db = null;
        PreparedStatement state = null;
        String sql = "DELETE FROM dept WHERE iddept = ?";
        try {
            db = new DB();
            state = db.getConnect().prepareStatement(sql);
            state.setInt(1, id);
            state.execute();
            db.getConnect().commit();
        } catch (Exception exception) {
            throw exception;
        } finally {
            if (state != null) {
                state.close();
            }
            if (db != null) {
                assert false;
                db.closeConnection();
            }
        }
    }
}
