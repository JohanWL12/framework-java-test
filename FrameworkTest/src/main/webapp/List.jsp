<%@ page import="java.util.List" %>
<%@ page import="com.frameworktest.Models.Dept" %>
<%@ page import="java.util.HashMap" %><%--
  Created by IntelliJ IDEA.
  User: johanwl
  Date: 14/11/2022
  Time: 15:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    HashMap<String, Object> data = (HashMap<String, Object>) request.getAttribute("data");
    List<Dept> deptList = (List<Dept>) data.get("data");
%>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>List Dept</h1>
<table>
    <tr>
        <th>id</th>
        <th>Nom</th>
    </tr>
    <% for (int i = 0; i < deptList.size(); i++) { %>
    <tr>
        <td><%out.print(deptList.get(i).getId());%></td>
        <td><%out.print(deptList.get(i).getNomDept());%></td>
    </tr>
    <% } %>
</table>
</body>
</html>
