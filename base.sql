\c postgres johanwl

drop database if exists dept;
create database dept;

\c dept postgres

create table dept(
    idDept serial primary key not null,
    nomDept varchar(50) not null
);

insert into dept(nomdept) values
('Finance'),
('Marketing'),
('Informatique'),
('Ressources Humaines'),
('Comptabilite'),
('Securite'),
('Logistique'),
('Commercial');